/** WARNING: this file has been automatically generated from C++ interfaces and classes, which exist in this package. */
// NOLINT (legal/copyright)

#ifndef COM_WUI_FRAMEWORK_LAUNCHER_INTERFACESMAP_HPP_  // NOLINT
#define COM_WUI_FRAMEWORK_LAUNCHER_INTERFACESMAP_HPP_

namespace Com {
    namespace Wui {
        namespace Framework {
            namespace Launcher {
                class Application;
                class Loader;
                namespace Structures {
                    class LauncherArgs;
                }
                namespace Utils {
                    class EnvironmentHandler;
                }
            }
        }
    }
}

#endif  // COM_WUI_FRAMEWORK_LAUNCHER_INTERFACESMAP_HPP_  // NOLINT
