/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef COM_WUI_FRAMEWORK_LAUNCHER_LOADER_HPP_
#define COM_WUI_FRAMEWORK_LAUNCHER_LOADER_HPP_

namespace Com::Wui::Framework::Launcher {
    /**
     * Loader class provides static application methods.
     */
    class Loader : public Com::Wui::Framework::XCppCommons::Loader {
     public:
        /**
         * Contains application logic entry method.
         * @param $argc An argument count.
         * @param $argv An array of arguments.
         * @return Returns exit code.
         */
        static int Load(const int $argc, const char *$argv[]) {
            Com::Wui::Framework::Launcher::Application application;
            return Com::Wui::Framework::XCppCommons::Loader::Load(application, $argc, $argv);
        }
    };
}

#endif  // COM_WUI_FRAMEWORK_LAUNCHER_LOADER_HPP_
