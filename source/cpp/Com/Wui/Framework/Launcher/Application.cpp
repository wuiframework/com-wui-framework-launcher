/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include <boost/tokenizer.hpp>

#include "sourceFilesMap.hpp"

#ifdef WIN_PLATFORM

#include <Tlhelp32.h>
#include <shellapi.h>

#endif

using Com::Wui::Framework::Launcher::Application;

using Com::Wui::Framework::Launcher::Structures::LauncherArgs;
using Com::Wui::Framework::XCppCommons::Utils::ArgsParser;
using Com::Wui::Framework::XCppCommons::Events::ThreadPool;
using Com::Wui::Framework::XCppCommons::Events::Args::EventArgs;
using Com::Wui::Framework::XCppCommons::Utils::IpcPipeObserver;
using Com::Wui::Framework::XCppCommons::Utils::LogIt;
using Com::Wui::Framework::XCppCommons::Enums::LogLevel;
using Com::Wui::Framework::XCppCommons::Enums::IOHandlerType;
using Com::Wui::Framework::XCppCommons::IOApi::IOHandlerFactory;
using Com::Wui::Framework::XCppCommons::EnvironmentArgs;
using Com::Wui::Framework::XCppCommons::System::IO::FileSystem;
using Com::Wui::Framework::XCppCommons::System::Process::Child;
using Com::Wui::Framework::XCppCommons::System::Process::SpawnOptions;
using Com::Wui::Framework::XCppCommons::System::Process::ExecuteOptions;
using Com::Wui::Framework::XCppCommons::Primitives::String;
using Com::Wui::Framework::XCppCommons::Primitives::ArrayList;
using std::ifstream;
using boost::filesystem::directory_iterator;
using boost::filesystem::exists;
using boost::filesystem::path;
using boost::filesystem::is_directory;
using boost::iequals;

namespace fs = boost::filesystem;

static const char APP_PATH[] = "./target";
static const char CHROMIUM_RE_PATH[] = "./wuichromiumre";
static const char NODEJS_RE_PATH[] = "./wuinodejsre";
static const char WUI_CONNECTOR_PATH[] = "./resource/libs/WuiConnector";
static unsigned int connectorPid = 0;
static string appName;  // NOLINT(runtime/string)
static fs::path cwd;

typedef struct {
 public:
    unsigned int pid;
    string name;
} ProcessInfo;

int Application::Run(int $argc, const char **$argv) {
    LauncherArgs args;

    cwd = EnvironmentArgs::getInstance().getExecutablePath();
    string appPath = (cwd / APP_PATH).normalize().make_preferred().string();
    if (!args.getTarget().empty()) {
        appPath = fs::path(args.getTarget()).normalize().make_preferred().string();
    }
    appName = EnvironmentArgs::getInstance().getExecutableName();

    LogIt::Info("Loading attributes from config file.");
    loadFromAppConfig(args);

    string tmpTarget = args.getTarget();
    if (ArgsParser::Parse(args, $argc, $argv) == 0) {
        LogIt::Info("Parse and merge attributes from CLI with previously loaded from config file.\n{0}", args);
        if (!args.getTarget().empty() && (args.getTarget() != tmpTarget)) {
            appPath = fs::path(args.getTarget()).normalize().make_preferred().string();
        }
        if (FileSystem::Exists(appPath)) {
            try {
                if (!fs::is_directory(appPath)) {
                    appPath = fs::path(appPath).parent_path().normalize().make_preferred().string();
                }
                string connectorPath = (fs::path(appPath) / WUI_CONNECTOR_PATH).normalize().make_preferred().string();

                if (FileSystem::Exists(connectorPath) &&
                    (args.getConnectorStart() || args.getConnectorStop() || args.getConnectorRestart() ||
                     args.getRunHost() || args.getRunChromiumRE())) {
                    std::vector<string> parameters = {"--target", appPath, "--host-port",
                                                      std::to_string(args.getPort())};

                    if (args.getRunHost()) {
                        parameters.emplace_back("--open-host");
                    }

                    bool servicesExists;
                    if (args.getConnectorRestart() || args.getConnectorStop()) {
                        servicesExists = sendMessageToConnector(appPath, args.getConnectorName(), "--stop");
                    } else {
                        servicesExists = sendMessageToConnector(appPath, args.getConnectorName(), parameters);
                    }

                    if (!args.getConnectorStop()) {
                        if (!servicesExists) {
                            string appArgs = "";
                            // exclude for all REs
                            if (!args.getRunChromiumRE() && !args.IsPluginRE()) {
                                appArgs = args.getAppArgs();
                                if (!appArgs.empty()) {
                                    appArgs = ArrayList::Join(splitAndQuoteArgs(appArgs));
                                }
                            }
                            openConnector(args.getConnectorName(), parameters, connectorPath, appArgs,
                                          args.IsConnectorElevate());
                        }

                        if (!(args.IsSingleton() && servicesExists)) {
                            if (args.getRunChromiumRE()) {
                                try {
                                    string tmp = args.getAppArgs();
                                    if (!tmp.empty()) {
                                        tmp = ArrayList::Join(splitAndQuoteArgs(tmp));
                                    }
                                    openChromiumRE(args.getConnectorName(), appPath, tmp);
                                } catch (std::exception &ex) {
                                    LogIt::Error(ex);
                                    openDefaultBrowser(appPath);
                                }
                            } else if (args.getRunNodejsRE()) {
                                runNodejsRE(args.getConnectorName(), appPath);
                            } else if (!servicesExists && !args.getRunHost() && !args.IsPluginRE()) {
                                openDefaultBrowser(appPath);
                            }
                        } else {
                            LogIt::Warning("Application already running in singleton instance.");
                        }
                    }

                    if (args.getRunHost()) {
                        LogIt::Info("Open connector HOST is required, and try open http://localhost:88 in default browser.");
                        openDefaultBrowser("http://localhost:88");
                    }
                } else if (!args.getConnectorStop()) {
                    LogIt::Info("Open default browser due to absence of other runtime-env configuration.");
                    openDefaultBrowser(appPath);
                }
            } catch (std::exception &ex) {
                LogIt::Error(ex);
            }
        } else {
            LogIt::Error("Application path not exist: \"{0}\"", appPath);
        }
    }
    return 0;
}

static void getProcessByName(const string &$name, std::vector<ProcessInfo> &$processes) {
#ifdef WIN_PLATFORM
    string fileName = $name + ".exe";
    HANDLE hSnapShot = CreateToolhelp32Snapshot(TH32CS_SNAPALL, 0);
    PROCESSENTRY32 pEntry{};
    pEntry.dwSize = sizeof(pEntry);
    BOOL hRes = Process32First(hSnapShot, &pEntry);
    $processes.clear();
    while (hRes != 0) {
        if (strcmp(pEntry.szExeFile, fileName.c_str()) == 0) {
            HANDLE hProcess = OpenProcess(PROCESS_QUERY_LIMITED_INFORMATION, 0, (DWORD)pEntry.th32ProcessID);
            if (hProcess != nullptr) {
                DWORD len = 1024;
                TCHAR buf[1024];
                QueryFullProcessImageName(hProcess, 0, buf, &len);
                $processes.push_back(ProcessInfo{.pid = static_cast<unsigned int>(pEntry.th32ProcessID), .name = buf});
                CloseHandle(hProcess);
            }
        }
        hRes = Process32Next(hSnapShot, &pEntry);
    }
    CloseHandle(hSnapShot);
#else
    path p("/proc");
    if (exists(p)) {
        directory_iterator end;
        for (directory_iterator it(p); it != end; ++it) {
            if (is_directory(it->path())) {
                try {
                    auto pid = boost::lexical_cast<unsigned int>(it->path().filename().c_str());
                    ifstream comm(string(it->path().c_str()) + "/comm");
                    if (comm.is_open()) {
                        string name;
                        getline(comm, name);

                        if (iequals(name, $name)) {
                            string fullProcPath;
                            ifstream cmdline(string(it->path().c_str()) + "/cmdline");
                            if (cmdline.is_open()) {
                                getline(cmdline, fullProcPath);
                                cmdline.close();
                            }
                            $processes.push_back(ProcessInfo{.pid = pid, .name = fullProcPath});
                        }
                        comm.close();
                    }
                } catch (const std::exception &$ex) {
                    LogIt::Error($ex);
                }
            }
        }
    }
#endif
    LogIt::Debug("AllProcesses: {0}", std::accumulate($processes.begin(), $processes.end(), string(),
                                                      [](string a, ProcessInfo &b) {
                                                          return a + ";" + b.name + ":" + std::to_string(b.pid);
                                                      }));
}

bool Application::sendMessageToConnector(const string &$appTarget, const string &$connectorName,
                                         const std::vector<string> &$args) {
    return sendMessageToConnector($appTarget, $connectorName, ArrayList::Join($args));
}

bool Application::sendMessageToConnector(const string &$appTarget, const string &$connectorName, const string &$message) {
    LogIt::Info("Sending message to connector: {0}\n\tfor target: \"{1}\"", $message, $appTarget);
    bool retVal = false;
    boost::filesystem::path appFilePath = $appTarget + "/" + WUI_CONNECTOR_PATH + "/" + $connectorName;
#ifdef WIN_PLATFORM
    appFilePath += ".exe";
#endif
    appFilePath = boost::filesystem::absolute(appFilePath).normalize().make_preferred();
    string appFileName = appFilePath.string();

    std::vector<ProcessInfo> processes;
    getProcessByName($connectorName, processes);
    if (!processes.empty()) {
        LogIt::Debug("Found {0} instances of {1}", static_cast<int>(processes.size()), $connectorName);
        std::for_each(processes.begin(), processes.end(),
                      [&retVal, $connectorName, &appFileName, $message](ProcessInfo &item) {
                          if (item.name == appFileName) {
                              LogIt::Debug("Connector found at path: {0}; pid: {1}; message {2}", appFileName.c_str(),
                                           item.pid, $message);
                              connectorPid = item.pid;
                              IpcPipeObserver ipcPipeObserver("WuiConnector", static_cast<int>(item.pid));
                              if (ipcPipeObserver.Initialize(false, 10000)) {
                                  ipcPipeObserver.Send($message);
                                  retVal = true;
                                  LogIt::Debug("Message send succeed.");
                              } else {
                                  LogIt::Error("Ipc pipe to {0} (pid: {1}) can not be opened.", $connectorName,
                                               std::to_string(item.pid));
                              }
                          }
                      });
    } else {
        LogIt::Error("Message can not be send, no connector process with name \"{0}\" found.", $connectorName);
    }
    return retVal;
}

string SearchExecutableInPath(const string &$fileName, const string &$path = "") {
#ifdef WIN_PLATFORM
    string path = $path;
    if (path.empty()) {
        path = Com::Wui::Framework::Launcher::Utils::EnvironmentHandler::getEnvironmentVariable("PATH");
        if (path.empty()) {
            throw std::runtime_error("Environment variable PATH not found");
        }
    }
    LogIt::Info("Searching in path: {0}", path);

    typedef boost::tokenizer<boost::char_separator<char>> tokenizer;
    boost::char_separator<char> sep(";");
    tokenizer tok(path, sep);
    for (tokenizer::iterator it = tok.begin(); it != tok.end(); ++it) {
        boost::filesystem::path p = *it;
        p /= $fileName;
        boost::array<string, 6> extensions = {"", ".exe", ".com", ".bat", ".cmd", ".msc"};
        for (auto &extension : extensions) {
            boost::filesystem::path p2 = p;
            p2 += extension;
            boost::system::error_code ec;
            bool file = boost::filesystem::is_regular_file(p2, ec);
            if (!ec && file && (SHGetFileInfoA(p2.string().c_str(), 0, 0, 0, SHGFI_EXETYPE) != 0u)) {
                return p2.string();
            }
        }
    }
    return "";
#else
    fs::path path;
    if ($path.empty()) {
        path = boost::process::search_path($fileName);
    } else {
        path = boost::process::search_path($fileName, {$path});
    }
    return path.string();
#endif
}

unsigned int Application::processStart(const string &$cmd, const std::vector<string> &$arguments,
                                       const string &$cwd, bool $isElevated) {
    unsigned int retVal = 0;
    string cwdBack;
    string cwd;

    auto envs = Com::Wui::Framework::Launcher::Utils::EnvironmentHandler::getEnvironmentVariables({{"global", true}});

    string cmd;
    if (!$cmd.empty()) {
        cmd = boost::filesystem::path($cmd).normalize().make_preferred().string();
        if (!boost::filesystem::exists(cmd)) {
            LogIt::Info("resolving in {0}", $cwd);
            cmd = SearchExecutableInPath(cmd, $cwd);
            if (cmd.empty()) {
                LogIt::Error("Command \"{0}\" not found.", $cmd);
                return retVal;
            }
            cmd = boost::filesystem::system_complete(cmd).string();
        }
    }

    try {
        if (!$isElevated) {
            auto child = Child::Spawn("\"" + cmd + "\"",
                                      $arguments,
                                      SpawnOptions(json({
                                                                {"detached", true},
                                                                {"cwd",      $cwd},
                                                                {"env",      envs}
                                                        })));

            retVal = static_cast<unsigned int>(child->getPid());
        } else {
            auto onExitHandler = [&retVal](int $exitCode, const string &$stdOut, const string &$stdErr) {
                if (!$stdOut.empty()) {
                    try {
                        retVal = boost::lexical_cast<unsigned int>(boost::trim_copy($stdOut));
                        LogIt::Info("Elevated process id: {0}", retVal);
                    } catch (std::exception &ex) {
                        LogIt::Error("Cannot cast elevate output pid to string.\n{0}", ex.what());
                    }
                } else {
                    LogIt::Warning("Process elevation was failed. See error description:\n{0}", $stdErr);
                }
            };

            string args = "";
            if ($arguments.size() > 0) {
                args = $arguments[0];
                if (args.size() > 1) {
                    args += " ";
                    for (int i = 1; i < static_cast<int>($arguments.size()); i++) {
                        args += $arguments[i];
                        if (i != static_cast<int>($arguments.size()) - 1) {
                            args += " ";
                        }
                    }
                }
            }

            string cmyy = "-Command \"(Start-Process -WindowStyle hidden '" + $cmd +
                          "' -ArgumentList '" + args + "' -Verb RunAs -PassThru).Id\"";

            LogIt::Info("Elevate environment path: {0}", string(getenv("PATH")));

            cmd = SearchExecutableInPath("powershell.exe");
            if (!cmd.empty()) {
                LogIt::Info("Starting elevated process: \n\t{0}", cmyy);

                ExecuteOptions options(boost::filesystem::system_complete($cwd).string(), envs);

                auto child = Child::Execute(cmd + " " + cmyy, options, onExitHandler);
                child->WaitForExit();
                LogIt::Debug("Elevated process \"{0}\" has been started.", $cmd);
            } else {
                LogIt::Error("Elevation failed: Can not locate powershell.exe in system path.");
            }
        }
    } catch (std::exception &ex) {
        LogIt::Error(ex);
        return retVal;
    }

    return retVal;
}

void Application::openDefaultBrowser(const string &$target) {
    LogIt::Info("Opening default browser for application: \"{0}\"", $target);
#ifdef WIN_PLATFORM
    processStart("cmd", std::vector<string>({"/c", "\"start " + $target + "\\index.html\""}), "");
#else
    processStart("/bin/bash", std::vector<string>({"-c", "\"xdg-open " + $target + "/index.html\""}), "");
#endif
}

void Application::openConnector(const string &$connectorName, const std::vector<string> &$parameters,
                                const string &$connectorDir, const string &$args, bool $isElevated) {
#ifdef WIN_PLATFORM
    string cmd = $connectorName + ".exe";
#else
    string cmd = "./" + $connectorName;
#endif

    auto args = $parameters;
    if (!$args.empty()) {
        args.emplace_back($args);
    }

    LogIt::Info("Opening connector for application with attributes:\n\t {0}\n\televated: {1}", ArrayList::Join(args), $isElevated);

    connectorPid = processStart(cmd, args, $connectorDir, $isElevated);
    if (connectorPid == 0) {
        LogIt::Error("Connector opening failed. Cannot create connector application process.");
    } else {
        LogIt::Info(R"(WuiConnector launched with name "{0}" and pid "{1}")", $connectorName, connectorPid);
    }
}

void Application::openChromiumRE(const string &$connectorName, const string &$target, const string &$args) {
    LogIt::Info("Opening chromiumRE for application: \"{0}\"", $target);
    string chromiumDir = (cwd / CHROMIUM_RE_PATH).normalize().make_preferred().string();
    if (fs::exists(chromiumDir) && !appName.empty()) {
        if (!fs::exists(fs::path(chromiumDir) / appName)) {
            LogIt::Warning(R"(WuiChromiumRE with launcher name "{0}" not found. Trying to find by native name "WuiChromiumRE.exe".)",
                           appName);
#ifdef WIN_PLATFORM
            appName = "WuiChromiumRE.exe";
#else
            appName = "WuiChromiumRE";
#endif
            if (!fs::exists(fs::path(chromiumDir) / appName)) {
                LogIt::Error("WuiChromiumRE not found in directory \"{0}\". \n Opening WuiChromiumRE skipped.", chromiumDir);
                return;
            }
        }
#ifdef WIN_PLATFORM
        string cmd = chromiumDir + "/" + appName;
#else
        string cmd = "./" + chromiumDir + "/" + appName;
#endif
        std::vector<string> args = {
                "--target=\"" + boost::filesystem::system_complete($target).string() + "\\index.html\"",
                "--connector-pid=" + std::to_string(connectorPid)
        };
        if (!$args.empty()) {
            args.emplace_back($args);
        }

        auto pid = processStart(cmd, args, chromiumDir);

        sendMessageToConnector($target, $connectorName, "--register-pid=" + std::to_string(pid));
    }
}

void Application::runNodejsRE(const string $connectorName, const string &$target) {
    LogIt::Info("Launching nodejsRE for application: \"{0}\"", $target);
    string nodejsDir = (cwd / NODEJS_RE_PATH).normalize().make_preferred().string();
    if (fs::exists(nodejsDir) && !appName.empty()) {
        if (!fs::exists(fs::path(nodejsDir) / appName)) {
            LogIt::Warning(R"(WuiNodejsRE with launcher name "{0}" not found. Trying to find by native name "WuiNodejsRE.exe".)",
                           appName);
#ifdef WIN_PLATFORM
            appName = "WuiNodejsRE.exe";
#else
            appName = "WuiNodejsRE";
#endif
            if (!fs::exists(fs::path(nodejsDir) / appName)) {
                LogIt::Error("WuiNodejsRE not found in directory \"{0}\". \n Launching WuiNodejsRE skipped.", nodejsDir);
                return;
            }
        }
#ifdef WIN_PLATFORM
        string cmd = nodejsDir + "/" + appName;
#else
        string cmd = "./" + nodejsDir + "/" + appName;
#endif
        auto pid = processStart(cmd,
                                {
                                        "resource/javascript/loader.min.js",
                                        "--connector-pid=" + std::to_string(connectorPid)
                                },
                                nodejsDir);

        sendMessageToConnector($target, $connectorName, "--register-pid=" + std::to_string(pid));
    }
}

void Application::loadFromAppConfig(LauncherArgs &$args) {
    using Com::Wui::Framework::XCppCommons::Utils::JSON;

    string appCfg = appName;
    unsigned long lf = appCfg.find_last_of('.');
    if (lf != string::npos) {
        appCfg.erase(lf);
    }

    appCfg += ".config.jsonp";

    appCfg = (cwd / appCfg).string();
    if (FileSystem::Exists(appCfg)) {
        try {
            json data = JSON::ParseJsonp(FileSystem::Read(appCfg));
            if (data.find("target") != data.end()) {
                $args.setTarget(data["target"]);
            }
            if (data.find("restart") != data.end()) {
                $args.setConnectorRestart(data["restart"]);
            }
            if (data.find("port") != data.end()) {
                $args.setPort(data["port"]);
            }
            if (data.find("runtimeEnv") != data.end()) {
                $args.setRuntimeEnvironment(data["runtimeEnv"]);
            }
            if (data.find("connectorName") != data.end()) {
                $args.setConnectorName(data["connectorName"]);
            }
            if (data.find("elevateConnector") != data.end()) {
                $args.setConnectorElevate(data["elevateConnector"]);
            }
            if (data.find("appArgs") != data.end()) {
                $args.setAppArgs(data["appArgs"]);
            }
            if (data.find("singleton") != data.end()) {
                $args.setSingleton(data["singleton"]);
            }
            LogIt::Info("Loaded application config:\n{0}", data.dump());
        } catch (boost::exception &ex) {
            LogIt::Error("Parsing of \"{0}\" failed.", appCfg);
        }
    } else {
        LogIt::Warning("Configuration file not found: \"{0}\". Starting with default configuration.", appCfg);
    }
}

std::vector<string> Application::splitAndQuoteArgs(const string &$args) {
    boost::char_separator<char> sep("", "=\" ", boost::drop_empty_tokens);
    boost::tokenizer<boost::char_separator<char>> tokenizer($args, sep);
    std::vector<string> origin;
    std::vector<std::pair<string, int>> map;

    string buf;
    boost::regex expr("-{1,2}[a-zA-Z0-9_\\-]+", boost::match_any);
    for (boost::tokenizer<boost::char_separator<char>>::iterator beg = tokenizer.begin(); beg != tokenizer.end(); ++beg) {
        if (boost::regex_match(*beg, expr)) {
            if (!buf.empty()) {
                map.push_back(std::make_pair(string(buf), std::count(buf.begin(), buf.end(), '"')));
                buf.clear();
            }
        }
        buf += *beg;
    }
    if (!buf.empty()) {
        map.push_back(std::make_pair(string(buf), std::count(buf.begin(), buf.end(), '"')));
    }

    bool append = false;
    std::pair<string, int> tmp;
    boost::regex exprQuote(R"([^\\]")");
    for (auto item : map) {
        if (append) {
            append = false;
            tmp.first += item.first;
            tmp.second += item.second;
        } else {
            tmp.first = item.first;
            tmp.second = item.second;
        }

        if (tmp.second != 0) {
            if ((tmp.second % 2 == 0) && boost::ends_with(boost::trim_right_copy(tmp.first), "\"")) {
                size_t start = tmp.first.find_first_of('"') + 1;
                size_t stop = tmp.first.find_last_of('"') - start;
                string rep = boost::regex_replace(tmp.first.substr(start, stop), exprQuote,
                                                  [&](const boost::smatch &$what) -> string {
                                                      return boost::replace_first_copy($what.str(), "\"", "\\\"");
                                                  }, boost::match_any);
                origin.emplace_back(boost::replace_all_copy(tmp.first, tmp.first.substr(start, stop), rep));
            } else {
                append = true;
            }
        } else {
            origin.emplace_back(tmp.first);
        }
    }

    return origin;
}
