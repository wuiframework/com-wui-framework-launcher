/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../sourceFilesMap.hpp"

namespace Com::Wui::Framework::Launcher::Structures {
    namespace po = boost::program_options;

    LauncherArgs::LauncherArgs() {
        po::options_description appDescription("Launcher options");

        appDescription.add_options()
                ("target", po::value<string>()->notifier(
                        boost::bind(&LauncherArgs::setTarget, this, _1)),
                 "Specify target application path.")
                ("port", po::value<int>()->notifier(
                        boost::bind(&LauncherArgs::setPort, this, _1)),
                 "Specify port.")
                ("connector-name", po::value<string>()->notifier(
                        boost::bind(&LauncherArgs::setConnectorName, this, _1)),
                 "Specify actual name of WuiConnector executable.")
                ("restart", po::value<bool>()
                         ->notifier(boost::bind(&LauncherArgs::setConnectorRestart, this, _1))
                         ->zero_tokens()
                         ->implicit_value(true),
                 "Use this to restart already running connector instance.")
                ("stop", po::value<bool>()
                         ->notifier(boost::bind(&LauncherArgs::setConnectorStop, this, _1))
                         ->zero_tokens()
                         ->implicit_value(true),
                 "Use this to stop already running connector instances.")
                ("runtime-env", po::value<string>()->notifier(
                        boost::bind(&LauncherArgs::setRuntimeEnvironment, this, _1)),
                 "Specify one of supported runtime environments.\n"
                         "Possible values:\n"
                         "    wuiconnector\n"
                         "    wuihost\n"
                         "    wuichromiumre\n"
                         "    wuieclipsere\n"
                         "    wuiideare\n"
                         "    wuinodejsre\n"
                         "    browser")
                ("elevate-connector", po::value<bool>()
                         ->notifier(boost::bind(&LauncherArgs::setConnectorElevate, this, _1))
                         ->zero_tokens()
                         ->implicit_value(true),
                 "Elevate connector process at startup. (Will ask user to allow that action before connector start)")
                ("app-args", po::value<string>()->notifier(
                        boost::bind(&LauncherArgs::setAppArgs, this, _1)),
                 "Specify arguments string for application. This application arguments will be emplaced back\n"
                         "to CLI arguments for launched application, ignored for 'runtime-env=\"browser\"'.\n"
                         "Arguments are applied only for 'end' application so ignored for connector if runtime-env=\"wuiconnector\" "
                         "is not used.\n"
                         "Ensure correct format of arguments i.e.\n"
                         "    --app-args=\"--target=\"<path to target>\"\n"
                         "    --connector-port=<port>\".")
                ("singleton", po::value<bool>()
                         ->notifier(boost::bind(&LauncherArgs::setSingleton, this, _1))
                         ->zero_tokens()
                         ->implicit_value(true),
                 "Use this to configuration to run application in singleton instance.");

        this->getDescription()->add(appDescription);
    }

    const string &LauncherArgs::getTarget() const {
        return this->target;
    }

    void LauncherArgs::setTarget(const string &$value) {
        this->target = $value;
    }

    int LauncherArgs::getPort() const {
        return this->port;
    }

    void LauncherArgs::setPort(const int $value) {
        this->port = $value;
    }

    void LauncherArgs::setConnectorName(const string &$value) {
        this->connectorName = $value;
    }

    const string &LauncherArgs::getConnectorName() const {
        return this->connectorName;
    }

    void LauncherArgs::setRuntimeEnvironment(const string &$value) {
        string key = boost::algorithm::to_lower_copy($value);
        if (key == "wuichromiumre") {
            this->setRunChromiumRE(true);
        } else if (key == "wuiconnector") {
            this->setConnectorStart(true);
        } else if (key == "wuieclipsere" || key == "wuiideare") {
            this->setConnectorStart(true);
            this->setPluginRE(true);
        } else if (key == "wuihost") {
            this->setRunHost(true);
        } else if (key == "browser") {
            this->setOpenBrowser(true);
        } else if (key == "wuinodejsre") {
            this->setConnectorStart(true);
            this->setRunNodejsRE(true);
        }
    }

    bool LauncherArgs::getConnectorStart() const {
        return this->connectorStart;
    }

    void LauncherArgs::setConnectorStart(const bool $value) {
        this->connectorStart = $value;
        if ($value) {
            this->runHost = false;
            this->runChromiumRE = false;
            this->openBrowser = false;
        }
    }

    bool LauncherArgs::getConnectorStop() const {
        return this->connectorStop;
    }

    void LauncherArgs::setConnectorStop(const bool $value) {
        this->connectorStop = $value;
        if ($value) {
            this->connectorRestart = false;
            this->connectorStart = false;
            this->runHost = false;
            this->runChromiumRE = false;
            this->openBrowser = false;
        }
    }

    bool LauncherArgs::getConnectorRestart() const {
        return this->connectorRestart;
    }

    void LauncherArgs::setConnectorRestart(const bool $value) {
        this->connectorRestart = $value;
    }

    bool LauncherArgs::getOpenBrowser() const {
        return this->openBrowser;
    }

    void LauncherArgs::setOpenBrowser(const bool $value) {
        this->openBrowser = $value;
        if ($value) {
            this->connectorStart = false;
            this->runHost = false;
            this->runChromiumRE = false;
        }
    }

    bool LauncherArgs::getRunHost() const {
        return this->runHost;
    }

    void LauncherArgs::setRunHost(const bool $value) {
        this->runHost = $value;
        if ($value) {
            this->connectorStart = true;
            this->runChromiumRE = false;
            this->openBrowser = false;
        }
    }

    bool LauncherArgs::getRunChromiumRE() const {
        return this->runChromiumRE;
    }

    void LauncherArgs::setRunChromiumRE(const bool $value) {
        this->runChromiumRE = $value;
        if ($value) {
            this->connectorStart = true;
            this->runHost = false;
            this->openBrowser = false;
        }
    }

    bool LauncherArgs::IsConnectorElevate() const {
        return this->connectorElevate;
    }

    void LauncherArgs::setConnectorElevate(bool $connectorElevate) {
        this->connectorElevate = $connectorElevate;
    }

    bool LauncherArgs::IsPluginRE() const {
        return this->isPluginRE;
    }

    void LauncherArgs::setPluginRE(bool $value) {
        this->isPluginRE = $value;
    }

    bool LauncherArgs::getRunNodejsRE() const {
        return this->runNodejsRE;
    }

    void LauncherArgs::setRunNodejsRE(bool $value) {
        this->runNodejsRE = $value;
    }

    const string &LauncherArgs::getAppArgs() const {
        return this->appArgs;
    }

    void LauncherArgs::setAppArgs(const string &$appArgs) {
        this->appArgs = $appArgs;
    }

    std::ostream &operator<<(std::ostream &$os, const LauncherArgs &$args) {
        json data = {
                {"target",           $args.target},
                {"port",             $args.port},
                {"connectorName",    $args.connectorName},
                {"connectorRestart", $args.connectorRestart},
                {"connectorStart",   $args.connectorStart},
                {"connectorStop",    $args.connectorStop},
                {"openBrowser",      $args.openBrowser},
                {"runHost",          $args.runHost},
                {"runChromiumRE",    $args.runChromiumRE},
                {"connectorElevate", $args.connectorElevate},
                {"isPluginRE",       $args.isPluginRE},
                {"runNodejsRE",      $args.runNodejsRE},
                {"appArgs",          $args.appArgs},
                {"singleton",        $args.singleton}
        };
        $os << data;
        return $os;
    }

    bool LauncherArgs::IsSingleton() const {
        return this->singleton;
    }

    void LauncherArgs::setSingleton(bool $singleton) {
        this->singleton = $singleton;
    }
}
