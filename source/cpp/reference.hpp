/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef COM_WUI_FRAMEWORK_LAUNCHER_HPP_  // NOLINT
#define COM_WUI_FRAMEWORK_LAUNCHER_HPP_

#include <unistd.h>

#include "../../dependencies/com-wui-framework-xcppcommons/source/cpp/reference.hpp"
#include "interfacesMap.hpp"
#include "Com/Wui/Framework/Launcher/sourceFilesMap.hpp"

// global-using-start
using std::vector;
// global-using-stop

// generated-code-start
#include "Com/Wui/Framework/Launcher/Application.hpp"
#include "Com/Wui/Framework/Launcher/Structures/LauncherArgs.hpp"
#include "Com/Wui/Framework/Launcher/Utils/EnvironmentHandler.hpp"
// generated-code-end


#include "Com/Wui/Framework/Launcher/Loader.hpp"

#endif  // COM_WUI_FRAMEWORK_LAUNCHER_HPP_  NOLINT
