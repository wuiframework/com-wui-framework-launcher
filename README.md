# com-wui-framework-launcher v2018.0.0

> Launcher for WUI Framework's applications

## Requirements

This application depends on the [WUI Builder](https://bitbucket.org/wuiframework/com-wui-framework-builder). 
See the WUI Builder requirements before you build this project.

This application has some special requirements:

* 7zip - for more information see the [readme.txt](resource/libs/7zip/readme.txt) file.
* winsdk - for more information see the [README.md](resource/libs/winsdk/README.md) file.

## Project Build

This project build is fully automated. For more information about the project build, 
see the [WUI Builder](https://bitbucket.org/wuiframework/com-wui-framework-builder) documentation.

An interface with batch/bash scripts is prepared for Windows/Linux users for all common project tasks:

* `install`
* `build`
* `clean`
* `test`
* `run`
* `hotdeploy`
* `docs`

> NOTE: All batch scripts are stored in the **./bin/batch** (or ./bin/bash for linux) sub-folder in the project root folder.

## Documentation

This project provides automatically generated documentation in [Doxygen](http://www.doxygen.org/index.html) 
from the C++ source by running the `docs` command from the {projectRoot}/bin/batch folder.

## History

### v2018.0.0
Added external link library according to XCppCommons update. Fixed application path resolving for override by CLI argument. Changed version format. 
### v2.2.0
Added support for singleton application. Fixed some minor bugs.
### v2.1.1
Internal configuration file formats migrated from XML to JSONP. Fixed issue with merge between CLI args and file options.
### v2.1.0
All platform dependent features ported also on linux.
### v2.0.3
Migration to novel Terminal and FileSystem handlers in XCppCommons.
### v2.0.2
Added support for plugin RE.
### v2.0.1
Syntax update. Support for WUI Connector custom name.
### v2.0.0
Namespaces refactoring.
### v1.2.0
Refactoring to C++ language
### v1.1.1
Added support for WUI IE Runtime Environment
### v1.1.0
Change of license from proprietary to BSD-3-Clause
### v1.0.0
Initial release

## License

This software is owned or controlled by NXP Semiconductors.
Use of this software is governed by the BSD-3-Clause License distributed with this material.
 
See the `LICENSE.txt` file distributed for more details.

---

Author Jakub Cieslar, 
Copyright (c) 2014-2016 [Freescale Semiconductor, Inc.](http://freescale.com/), 
Copyright (c) 2017-2018 [NXP](http://nxp.com/)
